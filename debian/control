Source: ros2-ament-cmake
Section: devel
Priority: optional
Maintainer: Debian Robotics Team <team+robotics@tracker.debian.org>
Uploaders: Timo Röhling <roehling@debian.org>,
           Jochen Sprickerhof <jspricke@debian.org>,
Build-Depends: debhelper-compat (= 13),
               cmake,
               dh-ros (>= 0.5.2),
               dh-sequence-python3,
               libgtest-dev <!nocheck>,
               python3-ament-package,
               python3-catkin-pkg,
               python3-distutils,
               python3-setuptools,
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/robotics-team/ros2-ament-cmake
Vcs-Git: https://salsa.debian.org/robotics-team/ros2-ament-cmake.git
Rules-Requires-Root: no
Homepage: https://github.com/ament/ament_cmake

Package: ament-cmake
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ament-cmake-core,
         ament-cmake-python,
         python3-ament-cmake-test,
Description: CMake build system for ROS 2 ament packages
 The ament build system is the most common way to build packages for ROS 2.
 ament_cmake provides the necessary tooling to build ament packages with
 CMake.
 .
 This package installs the most commonly used features of ament_cmake.

Package: ament-cmake-core
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         cmake,
         python3-ament-package,
         python3-catkin-pkg,
Description: CMake build system for ROS 2 ament package (Core functionality)
 The ament build system is the most common way to build packages for ROS 2.
 ament_cmake provides the necessary tooling to build ament packages with
 CMake.
 .
 This package installs the core functionality of ament_cmake.

Package: ament-cmake-googletest
Architecture: any
Multi-Arch: allowed
Depends: ${misc:Depends},
         ament-cmake-core,
         libgmock-dev,
         libgtest-dev,
         python3-ament-cmake-test,
Description: CMake build system for ROS 2 ament package (Googletest extension)
 The ament build system is the most common way to build packages for ROS 2.
 ament_cmake provides the necessary tooling to build ament packages with
 CMake.
 .
 This package adds support for GTest and GMock to ament_cmake.

Package: ament-cmake-python
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ament-cmake-core,
Description: CMake build system for ROS 2 ament package (Python extension)
 The ament build system is the most common way to build packages for ROS 2.
 ament_cmake provides the necessary tooling to build ament packages with
 CMake.
 .
 This package adds Python support to ament_cmake.

Package: ament-cmake-nose
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ament-cmake-core,
         python3-ament-cmake-test,
         python3-nose,
Description: CMake build system for ROS 2 ament package (Legacy nosetests extension)
 The ament build system is the most common way to build packages for ROS 2.
 ament_cmake provides the necessary tooling to build ament packages with
 CMake.
 .
 This package adds support for the deprecated Python Nosetests to ament_cmake.

Package: ament-cmake-pytest
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ament-cmake-core,
         python3-ament-cmake-test,
         python3-pytest,
Description: CMake build system for ROS 2 ament package (Pytest extension)
 The ament build system is the most common way to build packages for ROS 2.
 ament_cmake provides the necessary tooling to build ament packages with
 CMake.
 .
 This package adds support for Pytest to ament_cmake.

Package: python3-ament-cmake-test
Section: python
Architecture: all
Multi-Arch: foreign
Depends: ${python3:Depends},
         ${misc:Depends},
         ament-cmake-core,
         ament-cmake-python,
Description: Python 3 module for tests in ROS 2 ament packages
 The ament build system is the most common way to build packages for ROS 2.
 ament_cmake provides the necessary tooling to build ament packages with
 CMake.
 .
 This package adds support for unit tests to ament_cmake.

Package: python3-ament-cmake-google-benchmark
Section: python
Architecture: any
Multi-Arch: allowed
Depends: ${python3:Depends},
         ${misc:Depends},
         ament-cmake-core,
         ament-cmake-python,
         libbenchmark-dev,
Description: Python 3 module for Google Benchmark in ROS 2 ament packages
 The ament build system is the most common way to build packages for ROS 2.
 ament_cmake provides the necessary tooling to build ament packages with
 CMake.
 .
 This package adds support for Google Benchmark to ament_cmake.
